'use strict';

var exports = module.exports = {};

// assigning default calculation values
var taxRates = {
    defaultLowerTax : {
        tax: 18200,
        taxPerDollar: 0.19,
    },
    defaultMediumTax : {
        tax: 37000,
        taxPerDollar: 0.325,
    },
    defaultHigerTax : {
        tax: 87000,
        taxPerDollar: 0.37,
    },
    defaultAdvanceHigerTax : {
        tax: 180000,
        taxPerDollar: 0.45,
    },
}

/**
 * calculate income tax for given minimum and maximum amount
 * 
 * @param {annualSalary} annualSalary employee annual salary
 * @param {minAmount} minAmount minimum tax amount level to check
 * @param {maxAmount} maxAmount maximum tax amount level to calculate
 * @param {taxPerDollar} taxPerDollar increase amount per dollar
 * 
 * @return {number} annual income tax payments for selected range
 */
exports.calculateTax = (
    annualSalary,
    minAmount, 
    maxAmount, 
    taxPerDollar
) => {
    var taxAmount = 0;

    // check salary is greater than minimum salary
    if (parseInt(annualSalary) > minAmount) {
        // check maxAmount is not empty or 0
        if (maxAmount) {
            // check the annual salary is less than max amount
            if (parseInt(annualSalary) < maxAmount) {
                // find difference between extreme lower salary and annual salary 
                // then calculate by tax per dollar
                taxAmount = ((parseInt(annualSalary) - minAmount) * taxPerDollar);
            } else {
                // find difference between extreme lower salary and lower salary 
                // then calculate by tax per dollar
                taxAmount = ((maxAmount - minAmount) * taxPerDollar);
            }
        } else {
            // calculate tax more than $180000
            taxAmount = ((parseInt(annualSalary) - minAmount) * taxPerDollar);
        }
    }

    // return tax amount for lower salary
    return taxAmount;
};

/**
 * calculate annual income tax for less than $18,200 income
 * 
 * @param {number} annualSalary employee annual salary
 * 
 * @return {number} annual income tax payments for selected range
 */
exports.prepareExtremeLowerSalaryTax = (annualSalary) => {
    var taxAmount = 0;
    return taxAmount;
};

/**
 * calculate annual income tax for less than $37,00 income
 * 
 * @param {number} annualSalary employee annual salary
 * 
 * @return {number} annual income tax payments for selected range
 */
exports.prepareLowerSalaryTax = (annualSalary) => {
    // maximum tax calculation amount for medium salary earners
    var maxAmount = taxRates.defaultMediumTax.tax;
    // minimum tax calculation amount for medium salary earners
    var minAmount = taxRates.defaultLowerTax.tax;
    // calculate tax for the difference amount 
    var taxPerDollar = taxRates.defaultLowerTax.taxPerDollar;
    // calculate tax for the difference amount 
    return exports.calculateTax(annualSalary, minAmount, maxAmount, taxPerDollar);
};

/**
 * calculate annual income tax for less than $87,00 income
 * 
 * @param {number} annualSalary employee annual salary
 * 
 * @return {number} annual income tax payments for selected range
 */
exports.prepareMediumSalaryTax = (annualSalary) => {
    // maximum tax calculation amount for medium salary earners
    var maxAmount = taxRates.defaultHigerTax.tax;
    // minimum tax calculation amount for medium salary earners
    var minAmount = taxRates.defaultMediumTax.tax;
    // increment amount for earning extra dollar 
    var taxPerDollar = taxRates.defaultMediumTax.taxPerDollar;
    // calculate tax for the difference amount 
    return exports.calculateTax(annualSalary, minAmount, maxAmount, taxPerDollar);
};

/**
 * calculate annual income tax for less than $180,000 income
 * 
 * @param {number} annualSalary employee annual salary
 * 
 * @return {number} annual income tax payments for selected range
 */
exports.prepareHigherSalaryTax = (annualSalary) => {
    // maximum tax calculation amount for medium salary earners
    var maxAmount = taxRates.defaultAdvanceHigerTax.tax;
    // minimum tax calculation amount for medium salary earners
    var minAmount = taxRates.defaultHigerTax.tax;
    // increment amount for earning extra dollar 
    var taxPerDollar = taxRates.defaultHigerTax.taxPerDollar;
    // calculate tax for the difference amount 
    return exports.calculateTax(annualSalary, minAmount, maxAmount, taxPerDollar);
};

/**
 * calculate annual income tax for more than $180,000 income
 * 
 * @param {number} annualSalary employee annual salary
 * 
 * @return {number} annual income tax payments for selected range
 */
exports.prepareAdvanceHigherSalaryTax = (annualSalary) => {
    // maximum tax calculation amount for medium salary earners
    var maxAmount = 0;
    // minimum tax calculation amount for medium salary earners
    var minAmount = taxRates.defaultAdvanceHigerTax.tax;
    // increment amount for earning extra dollar 
    var taxPerDollar = taxRates.defaultAdvanceHigerTax.taxPerDollar;
    // calculate tax for the difference amount 
    return exports.calculateTax(annualSalary, minAmount, maxAmount, taxPerDollar);
}

/**
 * calculate annual income tax for different levels
 * 
 * @param {number} annualSalary employee annual salary
 * 
 * @return {number} annual total salary
 */
exports.calculateIncomeTaxTotal = (annualSalary) => {
    var salaryLevels = [
        exports.prepareExtremeLowerSalaryTax(annualSalary),
        exports.prepareLowerSalaryTax(annualSalary),
        exports.prepareMediumSalaryTax(annualSalary),
        exports.prepareHigherSalaryTax(annualSalary),
        exports.prepareAdvanceHigherSalaryTax(annualSalary),
    ];

    var total = 0;
    salaryLevels.map(amount => {
        total += amount;
    })

    return total;
}