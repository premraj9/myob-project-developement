'use strict';
var taxPayments = require('./taxPayments.js');
var exports = module.exports = {};

// assign const for total months
const TOTAL_MONTHS = 12;

/**
 * calculate monthly gross income from annual salary
 * 
 * @param {number} annualSalary  - annual empolyee salary
 * 
 * @return {number} monthly salary for pay period
 */
exports.prepareGrossIncome = (annualSalary) => {
    // divide annual salary by total months
    return annualSalary / TOTAL_MONTHS;
};

/**
 * calculate monthly income tax rate rate for month payment
 *  
 * @param {number} annualSalary - annual empolyee salary
 * 
 * @return {number} monthly income tax payment for pay month
 */
exports.prepareIncomeTax = (annualSalary) => {
    return taxPayments.calculateIncomeTaxTotal(annualSalary) / TOTAL_MONTHS;
};

/**
 * calculate monthly net income rate for month payment
 *  
 * @param {number} annualSalary - annual empolyee salary
 * 
 * @return {number} monthly net icome for pay month
 */
exports.prepareNetIncome = (annualSalary) => {
    return (
        exports.prepareGrossIncome(annualSalary) - exports.prepareIncomeTax(annualSalary)
    ) ;
};

/**
 * calculate monthly super rate for month payment
 *  
 * @param {number} annualSalary - annual empolyee salary
 * @param {number} superRate - monthly super rate
 * 
 * @return {number} monthly salary for pay period
 */
exports.prepareSuperRate = (annualSalary, superRate) => {
    // divide annual salary by total months
    return (exports.prepareGrossIncome(annualSalary) * superRate) / 100;
};

/**
 * calculate monthly income from annual salary
 * 
 * @param {number} annualSalary - annual empolyee salary
 * @param {number} superRatePercentage - monthly super rate
 * 
 * @return {array} total income details for employee payment month
 */
exports.prepareSalary = (annualSalary, superRatePercentage) => {
    // remove comma from amount passed and convert into integer
    var annualAmount = parseInt(annualSalary.split(',').join(''));
    // convert into integer
    var ratePercentage = parseInt(superRatePercentage);
    // get gross income of payment month
    const grossIncome = Math.round(exports.prepareGrossIncome(annualAmount));
     // get income tax for payment month
    const incomeTax = Math.round(exports.prepareIncomeTax(annualAmount));
     // get Net income for payment month
    const netIncome = Math.round(exports.prepareNetIncome(annualAmount));
     // get super rate for the month
    const superRate = Math.round(exports.prepareSuperRate(annualAmount, ratePercentage));

    // user object for pass calculated data
    const data = {
        grossIncome,
        incomeTax,
        superRate,
        netIncome
    }

    return data;
};
