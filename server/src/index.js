var express = require ('express');
var cors  = require ('cors');
var Salary = require('./modules/Salary');

// get express to connect 
const app = express();
app.use(cors());

// find url redirection
app.get('/', (request, response) => {
  // get url query params
  const { first_name, last_name, salary, super_rate, date } = request.query;
  try {
    if (parseInt(salary)) {
      // return json data to front end request
      return response.json({
        data: {
          firstName: first_name,
          lastName: last_name,
          income: Salary.prepareSalary(salary, super_rate),
          salary,
          date
        }
      });
    }
        // return 400 error
    response.statusCode = 400;
    return response.json({
      error: 'Data with result not found.',
    })
  } catch (e) {
    return response.json({
      error: 'Data with result not found.',
    })
  }
});


// find url redirection for calculating 
app.get('/salary/calculation', (request, response) => {
    // get url query params
  const { first_name, last_name, salary, super_rate, date } = request.query;
  try {
    if (parseInt(salary)) {
      // return json data to front end request
      return response.json({
        data: {
          firstName: first_name,
          lastName: last_name,
          income: Salary.prepareSalary(salary, super_rate),
          salary,
          date
        }
      });
    }
        // return 400 error
    response.statusCode = 400;
    return response.json({
      error: 'Data with result not found.',
    })
  } catch (e) {
    return response.json({
      error: 'Data with result not found.',
    })
  }
});

// listen to port 4000
app.listen(4000, () => {
  console.log('port 4000');
});