import React from 'react';
import * as Yup from 'yup';
import ContentLoader from 'react-content-loader';
import { compose, mapProps } from 'recompose';
import { Formik, Form } from 'formik';
import DateUtil from 'Lib/util/DateUtil';
import Link from 'Lib/elements/Link';
import Heading from 'Lib/elements/Heading';
import Paragraph from 'Lib/elements/Paragraph';
import InputCollection from 'Lib/collections/InputCollection';
import DatePickerCollection from 'Lib/collections/DatePickerCollection';

// @flow
type SalaryFormProps = {
  handleClick?: (values: mixed) => void,
};

type SalaryFormState = {
  isSaving: boolean,
};

/**
 * Validation schema used to validate the salary form
 *
 * @param {object} props - fromik actions and values information
 */
const validationSchema = props => {
  return Yup.object().shape({
    first_name: Yup.string('Please enter your first name')
      .required('Please enter your first name')
      .typeError('Please enter your first name'),
    last_name: Yup.string('Please enter your last name')
      .required('Please enter your last name')
      .typeError('Please enter your last name'),
    salary: Yup.number('Enter annual salary')
      .min(20000, 'Minimum Salary is $20000')
      .required('Enter annual salary')
      .typeError('Enter annual salary'),
    super_rate: Yup.number('Enter super rate')
      .min(1, 'Should be at least 1%')
      .required('Enter super rate')
      .typeError('Enter super rate'),
  });
}

/**
 * Employee Salary view.
 *
 * @author   Premraj Tharmrajah
 * 
 * @since    19 August 2018
 */
class SalaryForm extends React.Component<SalaryFormProps, SalaryFormState> {
  static defaultProps = {
    handleClick: () => {},
  };

  state: SalaryFormState = {
    isSaving: false,
  };

  /**
   * React LifeCycle Render View
   */
  render() {
    return (
      <div className="form-section-wrap margin-top-30">
        <Formik
          initialValues={{
            last_name: '',
            first_name: '',
            salary: 0,
            start_date: DateUtil.getTodaysDate(),
            super_rate: 0,
          }}
          validationSchema={validationSchema(this.props)}
          onSubmit={async values => {
            // Creating new employee salary
            const propPostData = {
              salary: values.salary,
              start_date: values.start_date,
              first_name: values.first_name,
              last_name: values.last_name,
              super_rate: values.super_rate,
            };

            // pass the form data to parents props to change the
            this.props.handleClick(propPostData);
       
          }}
          validateOnChange={false}
          render={props => {
             /**
             * set selected date to formik values set using setFieldValue
             *
             * @param string date : date selected using date time picker
             * @param string type : the date time frame (start or end)
             */
            const handleDatePicker = (date, type) => {
              props.setFieldValue('start_date', date);
            };
            
            const {
              values,
              errors,
              handleChange,
              handleSubmit,
            } = props;

            return (
              <Form>
                <div className="form-container">
                  <section className="section">
                    <div className="col-md-12">
                      <Heading text="Employee Salary Information" size={1} />
                      <Paragraph
                        text={`You're about create employee salary details. Please complete the following details`}
                        class="trial-form-heading"
                      />
                      <InputCollection
                        formGroupClass="col-md-8 col-sm-8 col-8 no-padding margin-top-20"
                        inputName="first_name"
                        inputClass="form-input-text"
                        paragraphClass="form-label text-sm text-grey-dark-medium"
                        paragraphText="Employee first name?"
                        inputSize={12}
                        inputValue={values.first_name}
                        onChange={handleChange}
                        errorMessage={errors.first_name}
                        placeholder="First Name"
                        required
                      />
                      <InputCollection
                        formGroupClass="col-md-8 col-sm-8 col-8 no-padding pull-left display-block"
                        paragraphClass="form-label text-sm text-grey-dark-medium"
                        paragraphText="Employee last name?"
                        inputName="last_name"
                        inputClass="form-input-text"
                        inputSize={12}
                        inputValue={values.last_name}
                        onChange={handleChange}
                        errorMessage={errors.last_name}
                        placeholder="Last Name"
                        required
                      />
                      <div className="col-md-8 col-sm-8 col-12 no-padding">
                        <DatePickerCollection
                          labelText="Employee salary payment period date selection."
                          labelClass="text-sm text-grey-dark-medium"
                          inputFromName="start_date"
                          inputToName="end_date"
                          inputSize={12}
                          startDate={values.start_date}
                          onChange={handleDatePicker}
                          dateFormat='DD-MM-YYYY'
                        />
                      </div>
                      <InputCollection
                        formGroupClass="col-md-4 col-sm-4 col-12 no-padding "
                        inputName="salary"
                        inputClass="form-input-text input--move-left"
                        inputType="number"
                        inputIconClass="fas fa-dollar-sign input--icon-left"
                        paragraphText="Employee Annual Salary?"
                        paragraphClass="form-label text-sm text-grey-dark-medium inline-block"
                        inputSize={4}
                        inputValue={values.salary}
                        onChange={handleChange}
                        errorMessage={errors.salary}
                        placeholder="Ex : 60000"
                        required
                      />
                      <InputCollection
                        formGroupClass="col-md-3 col-sm-3 col-12 margin-left-40 pull-left"
                        inputName="super_rate"
                        inputType="number"
                        inputClass="form-input-text"
                        paragraphText="Employee Super Rate?"
                        paragraphClass="form-label text-sm text-grey-dark-medium inline-block"
                        inputSize={4}
                        inputValue={values.super_rate}
                        onChange={handleChange}
                        errorMessage={errors.super_rate}
                        placeholder="Super Rate"
                        inputIconClass="fas fa-percent input--icon-right"
                        required
                      />
                    </div>
                  </section>
                  <section className="section-container-vertical">
                    <div className="margin-center width-220 padding-bottom-30">
                      <Link 
                        linkWrapperClass="agenda-load margin-top-30"
                        linkClass="w-100 btn btn--blue-medium"
                        linkText="GENERATE"
                        linkSize={0}
                        onClick={handleSubmit}
                      />
                    </div>
                  </section>
                </div>
              </Form>
            );
          }}
        />
      </div>
    );
  }
}

const enhance = compose(
  mapProps(({ ...rest }) => ({
    ...rest,
    data: {},
  })),
);

export default enhance(SalaryForm);

/**
 * 
 * @param {string} data - content loader
 */
export const SavingLoader = data => {
  return (
    <Link
      linkClass="btn btn-primary text-bold text-uppercase pull-right"
      linkText={
        <div className="col-md-12 inline-block no-padding text-center">
          {data.message}
          <ContentLoader
            width={100}
            height={15}
            speed={1}
            primaryColor="#E6A800"
            secondaryColor="#ffffff"
          >
            <rect x="5" y="10" rx="5" ry="5" width="5" height="5" />
            <rect x="12" y="10" rx="5" ry="5" width="5" height="5" />
            <rect x="19" y="10" rx="5" ry="5" width="5" height="5" />
          </ContentLoader>
        </div>
      }
      linkOffset={5}
      linkSize={2}
      responsive={false}
    />
  );
};
