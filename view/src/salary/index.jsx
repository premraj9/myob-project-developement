import React from 'react';
import isEmpty from 'lodash/isEmpty';
import AlertMessage from 'Lib/elements/AlertMessage';
import ImagePreview from 'Lib/elements/ImagePreview';
import Header from '../views/layouts/Header';
import Footer from '../views/layouts/Footer';
import SalaryForm from './form'
import SalaryView from './view'
import SalaryRequest from './api/SalaryRequest';

//import ImagePreview from 'Lib/elements/ImagePreview';
// @flow
/**
 * getting props information
 */
type SalaryMainProps = {
  onClick: () => void,
};

/**
 * getting State information
 */
type SalaryMainState = {
  employeeList: Array<mixed>,
  errorAlert: boolean,
  errorAlertText: string,
};

/**
 * Employee Salary view app page
 * 
 * For the employee user we use this UI to manage their income
 * 
 *
 * @author   Premraj Tharmarajah
 * @since    14 August 2018
 */
class SalaryMain extends React.Component<SalaryMainProps, SalaryMainState> {
  // this class default props information storage
  static defaultProps = {};
  
  // this class default state information storage
  state: AgendaState = {
    salaryData: [],
    errorAlert: false,
    errorAlertText: '',
  };

  /**
   * React Life Cycle Method for render view when state change 
   */
  shouldComponentUpdate(nextProps, nextState) {
    return this.state !== nextState;
  }

  /**
   * get salary calculated data from backend using annual salary
   * 
   * @param {array} formData Form input data
   */
  getSalary = async (formData) => {
    // assign form data into array
    const params = [
      `first_name=${formData.first_name}`,
      `last_name=${formData.last_name}`,
      `super_rate=${formData.super_rate}`,
      `salary=${formData.salary}`,
      `date=${formData.start_date}`,
    ];
    // request calculated income details
    const salaryData = await SalaryRequest.fetchSalaryCalculation(params);
    // check response have error
    if (salaryData.error) {
      this.setState({
        errorAlert: true,
        errorAlertText: salaryData.error,
      });

      window.scrollTo(0, 0);
    } else {
      // set data to the state
      this.setState({
        salaryData 
      });
    }
  }

  /**
   * React LifeCycle to render the view
   */
  render() {
    return (
      <div className="project-app">
        <div className="container-fluid">
          <div className="row">
            <Header />
            <main className="app-main">
                <div className="col-12 col-md-12">
                {this.state.errorAlert && (
                  <section className="section">
                    <div className="col-md-12 no-padding">
                      <AlertMessage
                        wrapperClass="padding-top-15 padding-bottom-15"
                        message={this.state.errorAlertText}
                        alertColor="danger"
                        showAlert
                      />
                    </div>
                  </section>
                )}
                <div className="col-6 col-md-6 display-inline-block">
                    <SalaryForm handleClick={this.getSalary} />
                    <ImagePreview 
                      imageSrc="./images"
                    />
                </div>
                <div className="salary-view col-6 col-md-6 display-inline-block">
                    { !isEmpty(this.state.salaryData) && !this.state.errorAlert 
                      ? <SalaryView salaryData={this.state.salaryData.data}/> 
                      : <ImagePreview imageWrapperClass="col-md-12" imageClass="col-12 col-md-12" imageSrc={'../../core/assets/images/nodatafound.png'} />
                    }
                </div>
              </div>
            </main>
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}

export default SalaryMain;
