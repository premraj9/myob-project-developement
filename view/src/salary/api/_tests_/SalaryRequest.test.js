import SalaryRequest from '../SalaryRequest';

// url query string data
const params = [
  `first_name=Premraj`,
  `last_name=Tharmarajah`,
  `super_rate=15`,
  `salary=60050`,
  `date=2018-08-21`,
];

// url query string data
const wrongParams = [
  `first_name=Premraj`,
  `last_name=Tharmarajah`,
  `super_rate=15`,
  `salary=`,
  `date=2018-08-21`,
];

// expected result data
const result = { 
  data: {
    date:"2018-08-21",
    firstName: "Premraj",
    income:{
      grossIncome: 5004, 
      incomeTax: 922, 
      superRate: 751, 
      netIncome: 4082
    },
    lastName: "Tharmarajah",
    salary:"60050", 
  }
};

/**
 * test json receiving same value as requested
 */
it('works with async/await', async () => {
    expect.assertions(result);
    const data = await SalaryRequest.fetchSalaryCalculation(params);
    expect(data).toEqual(result);
  });


/**
 * test result return object 
 */
it('tests error with async/await', async () => {
  expect.assertions(result);
  try {
    await SalaryRequest.fetchSalaryCalculation(wrongParams);
  } catch (e) {
    expect(e).toEqual({
      error: 'Data with result not found.',
    });
  }
});