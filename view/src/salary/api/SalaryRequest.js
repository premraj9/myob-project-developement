import 'es6-promise/auto';
import fetch from 'isomorphic-fetch';
import { defaultUrl } from 'Lib/config';
import * as ApiConstant from 'Core/constant/ApiResponse';
import * as DefaultConstant from 'Core/constant/DefaultConstant';
import * as UrlConstants from './UrlConstants';

/**
 * Employee Salary request response class
 * 
 * Calculate employee salary information using back end server
 *
 * @author   Premraj Tharmarajah
 * @since    19 August 2018
 */
class SalaryRequest {

  /**
   * pass parameter for calculation to server 
   * 
   * Using annual salary get monthly payment
   *  
   * @param {array} params url passing parameter
   */
  static fetchSalaryCalculation(params) {
    // get connection URl Path
    let url = `${defaultUrl}${UrlConstants.SALARY_CALCULATION}`;
    // check the url contain param length and add new params
    if (params.length) {
      url += url.indexOf(DefaultConstant.SYMBOL_QUESTION) === -1 
        ? DefaultConstant.SYMBOL_QUESTION 
        : DefaultConstant.SYMBOL_AND;
      url += params.join(DefaultConstant.SYMBOL_AND);
    }
    
    // request url response
    const request = new Request(url, {
      method: 'GET',
    });

    // return fetch data to react front end
    return fetch(request)
      .then((response) => {
        if (
          response.status === ApiConstant.STATUS_CODE_SUCCESS
        ) {
          return response.json();
        }
        return {
          error: 'Data with result not found.'
        };
      })
      .catch(error =>  { 
        return {
          error: 'Sorry failed to connect with the server'
        };
      });
  }
}

export default SalaryRequest;
