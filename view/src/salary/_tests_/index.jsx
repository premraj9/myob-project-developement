
import MainIndex from "../index";

test('render a label', () => {
    const wrapper = shallow(
        <MainIndex>Hello Jest!</MainIndex>
    );
    expect(wrapper).toMatchSnapshot();
});

// test('render a small label', () => {
//     const wrapper = shallow(
//         <MainIndex small>Hello Jest!</MainIndex>
//     );
//     expect(wrapper).toMatchSnapshot();
// });

// test('render a grayish label', () => {
//     const wrapper = shallow(
//         <MainIndex light>Hello Jest!</MainIndex>
//     );
//     expect(wrapper).toMatchSnapshot();
// });