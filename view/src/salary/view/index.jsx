import React from 'react';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import DateUtil from 'Lib/util/DateUtil';
import * as DateConstant from 'Core/constant/DateConstant';
import Link from 'Lib/elements/Link';
import Heading from 'Lib/elements/Heading';
import Paragraph from 'Lib/elements/Paragraph';

// @flow
/**
 * getting props information
 */
type SalaryViewProps = {
  salaryData: Array<mixed>,
};

/**
 * getting State information
 */
type SalaryViewState = {
  employeeList: Array<mixed>
};

/**
 * Employee Salary view page
 * 
 * For the employee user we use this UI to manage their income
 * 
 * @author   Premraj Tharmarajah
 * @since    14 August 2018
 */
class SalaryView  extends React.Component<SalaryViewProps, SalaryViewState> {
  // this class default props information storage
  static defaultProps = {};
  
  // this class default state information storage
  state: AgendaState = {
    salaryData: [],
  };

  /**
   * React Life Cycle Method for render view when state change 
   */
  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(this.state, nextState) || !isEqual(this.props, nextProps);
  }

  /**
   * React LifeCycle to render the view
   */
  render() {
    const startDate = DateUtil.getMonthStartDate(this.props.salaryData.income.date);
    const endDate = DateUtil.getMonthEndDate(this.props.salaryData.income.date);

    return (
      <div className="form-container">
        <section className="section">
          <Heading text="Salary Genereated View" size={1} />
          <Paragraph
            text={`Employees salary generated view after deductions`}
            class="trial-form-heading"
          />
          <div className="col-md-12 payment-slip-main">
            <div className="col-md-12 no-padding payment-slip-header">
                <label className="display-block text-bold"> {this.props.salaryData.firstName} {this.props.salaryData.lastName} </label>
            </div>
            <div className="col-md-12 no-padding payment-slip-pay">
                <label className="margin-top-5 "> 
                  Payment Period:  
                   { DateUtil.convertDateToFormat(startDate, null, DateConstant.DATE_MONTH_FORMAT_DISPLAY)} -  
                   { DateUtil.convertDateToFormat(endDate, null, DateConstant.DATE_MONTH_FORMAT_DISPLAY)}
                </label>
                <label className="margin-top-5 margin-right-10 float-right"> Total Earnings: ${this.props.salaryData.income.grossIncome} </label>
            </div>

            <div className="col-md-12 no-padding payment-slip-list">
                <label className="margin-top-5 "> Gross Income</label>
                <label className="margin-top-5 margin-right-10 float-right"> ${this.props.salaryData.income.grossIncome} </label>
            </div>
            <div className="col-md-12 no-padding payment-slip-list">
                <label className="margin-top-5 "> Income Tax</label>
                <label className="margin-top-5 margin-right-10 float-right"> ${this.props.salaryData.income.incomeTax} </label>
            </div>
            <div className="col-md-12 no-padding payment-slip-list">
                <label className="margin-top-5 "> Net Income</label>
                <label className="margin-top-5 margin-right-10 float-right"> ${this.props.salaryData.income.netIncome} </label>
            </div>
            <div className="col-md-12 no-padding payment-slip-list">
                <label className="margin-top-5 "> Super Rate</label>
                <label className="margin-top-5 margin-right-10 float-right"> ${this.props.salaryData.income.superRate} </label>
            </div>
            <div className="col-md-12 no-padding payment-slip-list-total">
                <label className="margin-top-5 text-bold "> Total</label>
                <label className="margin-top-5 margin-right-10 float-right"> ${this.props.salaryData.income.netIncome} </label>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default SalaryView;
