import mapValues from "lodash/mapValues";
import Salary from ".././salary";
import App404Component from "./App404Component";

const componentClassName = [
  {
    Component: Salary,
    Path: "",
    Default: true,
    Exact: false
  },
  // {
  //   Component: App404Component,
  //   Path: '*',
  //   Default: false,
  //   Exact: true,
  // },
];

const reportRoutes = [];

mapValues(componentClassName, (route, i) => {
  reportRoutes[reportRoutes.length] = {
    Path: `/${route.Path}`,
    Component: route.Component,
    Default: route.Default,
    exact: route.Exact
  };
});

// to={{ pathname: '/user/bob', query: { showAge: true } }}
export default reportRoutes;
