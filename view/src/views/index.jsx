import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Paths from './Paths';

/**
 * To know about react router please find in the following link
 * @see https://reacttraining.com/react-router/
 */
const RouteWithSubRoutes = route => {
  return (
    <Route
      exact
      path={route.Path}
      render={props => (
        <route.Component {...props} propertyId={route.props.propertyId} />
      )}
    />
  );
};

/**
 * getting props information
 */
type RoutesProps = {
  logoutApp: () => void,
};

/**
 * React storing the path URL's for changing the page
 * For more information check react-router-dom
 * Pages changing using url param inside the APP
 *
 * @author   Premraj Tharmarajah
 * @since    16 August 2018
 */
class MainIndex extends React.Component<RoutesProps> {
  static defaultProps = {};

  /**
   * React Life Cycle Method for run after view rendered 
   */
  componentDidMount() {
    //check for the session time out
  }

  /**
   * React LifeCycle to render the view
   */
  render() {
    /**
     * Router for changing the page
     */
    return (
      <Router keyLength={5}>
        <Switch>
          <div className="m-app-01">
          {Paths.map((route, i) => (
            <RouteWithSubRoutes
              key={`key-${i}`}
              {...route}
              props={this.props}
            />
          ))}
          </div>
        </Switch>
      </Router>
    );
  }
}

export default MainIndex;
