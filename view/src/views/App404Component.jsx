import React from 'react';

/**
 * Path Not Found Page View
 * 
 * @author   Premraj Tharmarajah
 * @since    12 August 2018
 */
function App404Component() {
    return (
      <div className="row">
            No Page Found
      </div>
    );
}

export default App404Component;
