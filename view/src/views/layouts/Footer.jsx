import React from 'react';
import Label from "Lib/elements/Label";

/**
 * Main index page footer view function
 *
 * @author   Premraj Tharmarajah
 * @since    14 August 2018
 */

function Footer() {
  return (
    <footer className="mobile-app-footer">
      <div className="col-12 margin-top-30"> 
        <Label
          labelText={
            <span>
              POWERED BY <strong>PREMRAJ</strong>
            </span>
          }
          labelClass="w-100"
          labelTitle="POWERED BY PREMRAJ"
        />
      </div>
    </footer>
  );
}

export default Footer;
