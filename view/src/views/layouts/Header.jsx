import React from 'react';
import Label from "Lib/elements/Label";

/**
 * Main index page header view function
 *
 * @author   Premraj Tharmarajah
 * @since    14 August 2018
 */
function Header()  {
    return (
      <header className="app-header">
        {
          /*
            @TODO uncomment in future for linking
            <Icon 
              iconClass="fas fa-search"
              wrapperClass="menu-wrap"
            />
            <Icon 
              iconClass="fas fa-plus"
              wrapperClass="menu-wrap"
            />
            <Icon 
              iconClass="far fa-calendar-alt"
              wrapperClass="menu-wrap"
            /> 
          */
        }
        <Label
          iconClass="fas fa-lg fa-file-invoice-dollar"
          labelText="Employee Salary"
          labelClass="text-center text-bold text-lg margin-top-20 margin-left-50"
        />
      </header>
    );
}

export default Header;