import React from "react";
import ReactDOM from "react-dom";
import MainIndex from "./views/index";
import "Core/assets/scss/main.scss";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(<MainIndex />, document.getElementById("root"));
registerServiceWorker();