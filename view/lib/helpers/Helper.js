/**
 * Check the value is  number | boolean and it has the string name
 * then convert it proper data type
 *
 * @param {string} value - the value to check it is boolean or number
 *
 * @return string coverted into proper data type
 */
export function dataTypeValidation(value) {
  if (typeof value === "boolean") {
    // check the value is type of boolean
    return value;
  } else if (typeof value === "string" && value === "true") {
    // check the value is type of string true the return boolean true
    return true;
  } else if (typeof value === "string" && value === "false") {
    // check the value is type of string false the return boolean false
    return false;
  } else if (typeof value === "string" && !isNaN(value)) {
    // check the value is type of string but it has to be number
    return parseInt(value, 10);
  }
  // if the value not in the condition check the pass the default value
  return value;
}