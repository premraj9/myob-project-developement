import { dataTypeValidation } from '../Helper';

/**
 * test result return true 
 */
it('convert string into boolean true', () => {
  const data = dataTypeValidation('true');
  expect(data).toEqual(true);
});


/**
 * test result return false 
 */
it('convert string into boolean false', () => {
  const data = dataTypeValidation('false');
	expect(data).toEqual(false);
});

/**
 * test result return false 
 */
it('convert string  number into int', () => {
  const data = dataTypeValidation('100');
	expect(data).toEqual(100);
});