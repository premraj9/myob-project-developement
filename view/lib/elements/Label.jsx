// @flow
import React from 'react';
import cn from 'classnames';
import Icon from './Icon';

type LabelProps = {
  wrapperClass?: string,
  labelText: string,
  labelClass?: string,
  labelTitle?: string,
  labelFor?: string,
  iconClass?: string,
  dataTip?: string,
  dataFor?: string,
};

/**
 * Show the label tag
 *
 * @author Premraj tharmarajah
 * @since  16 August 2018
 */
function Label(props: LabelProps) {
  let icon = '';
  // check props has the iconClass
  if (props.iconClass) {
    icon = <Icon iconClass={props.iconClass} />;
  }

  const classNames = cn(props.labelClass, 'control-label');
  if (props.wrapperClass !== '') {
    return (
      <div className={props.wrapperClass}>     
        <label
          className={classNames}
          title={props.labelTitle}
          htmlFor={props.labelFor}
          data-tip={props.dataTip}
          data-for={props.dataFor}
        >
          {icon} {props.labelText}
        </label>
    </div>
    );
  }
  
  return (
    <label
      className={classNames}
      title={props.labelTitle}
      htmlFor={props.labelFor}
      data-tip={props.dataTip}
      data-for={props.dataFor}
    >
      {icon} {props.labelText}
    </label>
  );
}

Label.defaultProps = {
  wrapperClass: '',
  labelClass: '',
  labelTitle: '',
  labelFor: 'inputText',
  dataTip: '',
  dataFor: '',
  iconClass: '',
};

export default Label;
