// @flow
import React from 'react';

type imageProps = {
  imageWrapperClass?: string,
  imageClass?: string,
  imageSrc: string,
  imageAltText?: string,
  imageLink?: boolean,
};

/**
 * Show the image in html img tag
 *
 * @author Premraj tharmarajah
 * @since  26 August 2018
 */
function ImagePreview(props: imageProps)  {
    // if onclick on image redirect to different page
    if (props.imageLink) {
      return (
        <a href={props.imageLink} className={props.imageWrapperClass}>
          <img
            className={props.imageClass}
            src={props.imageSrc}
            alt={props.imageAltText}
          />
        </a>
      );
    }

    return (
      <div className={props.imageWrapperClass}>
        <img
          className={props.imageClass}
          src={props.imageSrc}
          alt={props.imageAltText}
        />
      </div>
    );
}

ImagePreview.defaultProps = {
  imageWrapperClass: '',
  imageClass: '',
  imageSrc: '',
  imageAltText: '',
  imageLink: false,
};


export default ImagePreview;
