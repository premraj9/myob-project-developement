import React from 'react';

type propTypes = {
  iconClass: string,
  wrapperClass?: string,
};

function Icon(props: propTypes) {
  if (props.wrapperClass) {
    return (
      <span className={props.wrapperClass}>
        <i className={props.iconClass} />
      </span>
    )
  }

  return <i className={props.iconClass} />;
}

Icon.defaultProps = {
  iconClass: '',
  wrapperClass: '',
};

export default Icon;
