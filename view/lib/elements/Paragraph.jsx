import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

type paragraphProps = {
  class?: string,
  text: string,
  type?: string,
  wrapperClass?: string,
  iconClass?: string,
};

const defaultProps = {
  type: 'p',
  class: '',
  iconClass: '',
  wrapperClass: '',
};

function Paragraph(props: paragraphProps) {
  const TagName = `${props.type}`;
  let icon = '';
  if (props.iconClass) {
    icon = <Icon iconClass={props.iconClass} />;
  }
  if (props.wrapperClass !== '') {
    return (
      <div className={props.wrapperClass}>
        <TagName className={props.class}>
          {icon} {props.text}
        </TagName>
      </div>
    );
  }

  return (
    <TagName className={props.class}>
      {icon} {props.text}
    </TagName>
  );
}

Paragraph.defaultProps = defaultProps;

export default Paragraph;
