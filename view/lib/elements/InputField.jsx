import React from 'react';
import cn from 'classnames';
import Icon from './Icon';
import Label from './Label';

// @flow
type InputFieldProps = {
  inputName: string,
  inputType?: 'date' | 'email' | 'number' | 'password' | 'text' | 'url',
  inputValue?: string | number,
  inputClassName?: string,
  inputSize?: number,
  iconClass?: string,
  disabled?: boolean,
  placeholder?: string,
  wrapperClass?: string,
  errorMessage: string,
  required?: boolean,
  autoComplete?: 'current-password' | 'on' | 'off' | 'username',
  errorMessage?: string,
  hasError?: boolean,
  onChange: ({ event: SyntheticInputEvent<>, value: string }) => void,
  onBlur?: ({ event: SyntheticFocusEvent<>, value: string }) => void,
  onFocus?: ({ event: SyntheticFocusEvent<>, value: string }) => void,
};

type InputFieldState = {
  focused: boolean,
  errorIsOpen: boolean,
  value: string | number,
};

class InputField extends React.Component<InputFieldProps, InputFieldState> {
  static defaultProps = {
    inputType: 'text',
    inputValue: '',
    inputClassName: 'col-12',
    inputSize: 12,
    disabled: false,
    placeholder: '',
    wrapperClass: '',
    errorMessage: '',
    iconClass: '',
    required: false,
    autoComplete: 'off',
    onBlur: () => {},
    onFocus: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      errorIsOpen: false,
      value: this.props.inputValue,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.inputValue !== prevProps.inputValue) {
      this.updateState(this.props);
    }
  }

  updateState(props) {
    if (props.inputValue || props.inputValue === '') {
      this.setState({ value: props.inputValue });
    }
  }

  handleChange = event => {
    if (event.target instanceof HTMLInputElement) {
      this.setState({
        value: event.target.value,
      });

      this.props.onChange(event);
    }
  };

  handleBlur = (event: SyntheticFocusEvent<>) => {
    if (this.props.errorMessage) {
      this.setState({ errorIsOpen: false });
    }

    if (event.target instanceof HTMLInputElement && this.props.onBlur) {
      this.props.onBlur({
        event,
        value: event.target.value,
      });
    }
  };

  handleFocus = (event: SyntheticFocusEvent<>) => {
    if (this.props.errorMessage) {
      this.setState({ errorIsOpen: true });
    }
    if (event.target instanceof HTMLInputElement && this.props.onFocus) {
      this.props.onFocus({
        event,
        value: event.target.value,
      });
    }
  };

  render() {
    const {
      autoComplete,
      inputType,
      errorMessage,
      iconClass,
      required,
    } = this.props;
    /*
     * type='number' doesn't work on ios safari without a pattern
     * https://stackoverflow.com/questions/14447668/input-type-number-is-not-showing-a-number-keypad-on-ios
     */
    const pattern = inputType === 'number' ? '\\d*' : undefined;
    const wrapperClass = cn(
      `col-sm-${this.props.inputSize}`,
      `col-md-${this.props.inputSize}`,
      this.props.wrapperClass,
      errorMessage ? 'input--error' : '',
      required ? 'input--required' : ''
    );

    const icon = iconClass ? <Icon iconClass={`input--icon-warp ${iconClass}`} /> : '';
    const inputclass = cn('input--text', this.props.inputClassName);

    return (
      <span className={wrapperClass}>
        <input
          aria-describedby={
            errorMessage && this.state.focused
              ? `${inputId}-gestalt-error`
              : null
          }
          aria-invalid={errorMessage ? 'true' : 'false'}
          type={this.props.inputType}
          name={this.props.inputName}
          id={this.props.inputName}
          autoComplete={autoComplete}
          pattern={pattern}
          className={inputclass}
          value={this.state.value ? this.state.value : ''}
          placeholder={this.props.placeholder}
          disabled={this.props.disabled}
          onBlur={this.handleBlur}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
        />
        {icon}
        {required && <Icon iconClass="fas fa-asterisk error--icon" />}
        {errorMessage && (
          <Label labelText={errorMessage} labelClass="label--error" />
        )}
      </span>
    );
  }
}

export default InputField;
