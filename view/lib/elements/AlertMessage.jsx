import React from 'react';
import cn from 'classnames';
import Link from './Link';
import Label from './Label';

// @flow
type AlertPropType = {
  wrapperClass?: string,
  message?: string,
  iconClass?: string,
  alertColor?: string,
  textColor?: string,
  closeClick?: (param: number | string) => void,
  showAlert?: boolean,
  backgroundColor?: boolean,
};

export default class AlertMessage extends React.Component<AlertPropType> {
  static defaultProps = {
    linkTo: '',
    linkClass: '',
    message: '',
    iconClass: 'fas fa-exclamation-circle',
    alertColor: 'primary',
    textColor: 'text-white',
    wrapperClass: '',
    showAlert: true,
    backgroundColor: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      showAlert: this.props.showAlert,
    };
  }

  closeClick = (event: SyntheticEvent<HTMLButtonElement>) => {
    if (this.props.closeClick && typeof this.props.closeClick === 'function') {
      this.props.closeClick(event);
    }

    this.setState({ showAlert: false });
  };

  render() {
    if (!this.state.showAlert) {
      return false;
    }

    const classNames = cn(
      'cs-alertcontainer',
      'col-12',
      this.props.backgroundColor ? `bg-${this.props.alertColor}` : '',
      this.props.wrapperClass
    );

    return (
      <div className={classNames}>
        <Label
          labelClass={`${this.props.textColor} no-margin`}
          iconClass={this.props.iconClass}
          labelText={this.props.message}
        />
        {typeof this.props.closeClick === 'function' && (
          <Link
            linkClass={`pull-right ${this.props.textColor}`}
            onClick={this.closeClick}
            iconClass={'fa fa-lg fa-close'}
          />
        )}
      </div>
    );
  }
}
