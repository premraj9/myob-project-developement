// @flow
import React from 'react';
import Icon from './Icon';

// declare props
type headingProps = {
  text: string,
  size?: number,
  className?: string,
  iconClass?: string,
};

const defaultProps = {
  size: 1,
  className: '',
  iconClass: '',
};

// Create Heading component and export it
function Heading(props: headingProps) {
  const Head = `h${props.size}`;
  const icon = props.iconClass ? <Icon iconClass={props.iconClass} /> : '';
  return (
    <Head className={props.className}>
      {icon}
      {props.text}
    </Head>
  );
}
Heading.defaultProps = defaultProps;
export default Heading;
