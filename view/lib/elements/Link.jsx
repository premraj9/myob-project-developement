import React from 'react';
import Classnames from 'classnames';
import Icon from './Icon';

// @flow
/**
 * @property onClick - Defines a function to run when the component clicked
 * @property linkTo - Used to redirect to internal components
 * @property externalLink - Used to link to external addresses
 */
type LinkProps = {
  linkWrapperClass?: string,
  linkClass?: string,
  linkText?: string,
  linkSize?: number,
  iconClass?: string,
  target?: string,
  parameter?: number | string,
  linkOffset?: number,
  onClick?: (param: number | string) => void,
  externalLink?: string,
  iconLocation?: string,
  responsive?: boolean,
};

type LinkStates = {
  offset?: string | empty,
};

class Link extends React.Component<LinkProps, LinkStates> {
  static defaultProps = {
    linkWrapperClass: '',
    linkClass: 'col-3 offset-3 btn btn-primary',
    linkText: '',
    parameter: '',
    externalLink: '',
    iconClass: '',
    target: '_blank',
    iconLocation: 'left',
    linkOffset: 0,
    linkSize: 0,
    responsive: true,
  };

  constructor(props: LinkProps) {
    super(props);
    this.state = {
      offset: '',
    };
  }

  componentDidMount() {
    this.updateState();
  }

  onClick = (event: SyntheticInputEvent<HTMLButtonElement>) => {
    this.props.onClick(event, this.props.parameter);
  };

  updateState = () => {
    // if the column offset and is greater than 0 then adding a bootstrap offset class to it
    if (this.props.linkOffset && this.props.linkOffset !== 0) {
      this.setState({ offset: ` col-offset-${this.props.linkOffset}` });
    }
  };

  view = (linkView) => {
    if (this.props.linkWrapperClass) {
      return (
        <div className={this.props.linkWrapperClass} >
          {linkView}
        </div>
      );
    }
    return linkView;
  }

  render() {
    // check this class receiving any icons
    let iconTag = '';
    if (this.props.iconClass) {
      iconTag = <Icon iconClass={this.props.iconClass} />;
    }
    const rightIcon = this.props.iconLocation === 'right' ? iconTag : '';
    const leftIcon = this.props.iconLocation === 'left' ? iconTag : '';

    const classNames = Classnames(
      this.props.linkClass,
      this.state.offset ? this.state.offset : '',
      this.props.linkSize ? this.props.responsive
        ? `col-${this.props.linkSize}`
        : `width-${this.props.linkSize}`
        : ''
    );

    let linkView = (
      <a
        className={classNames}
        onClick={this.onClick}
        role={'link'}
        tabIndex={'0'}
        onKeyPress={this.onClick}
      >
        {leftIcon}
        {this.props.linkText}
        {rightIcon}
      </a>
    );

    // onclick redirect page to different url
    if (this.props.externalLink) {
      linkView = (
        <a
          className={classNames}
          target={this.props.target}
          href={this.props.externalLink}
          role={'link'}
          tabIndex={'0'}
        >
          {leftIcon}
          {this.props.linkText}
          {rightIcon}
        </a>
      );
    }

    return (
      this.view(linkView)
    )
  }
}

export default Link;
