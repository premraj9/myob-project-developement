jest.unmock('../Link');

import React from 'react';
import { shallow } from 'enzyme';
import Link from '../Link';

describe('<Link />', () => {
  it('render without exploding', () => {
    expect(shallow(
        <Link 
            linkText="testing" 
        />
    ).length).toEqual(1);
  });
});