jest.unmock('../Button');

import React from 'react';
import { mount, shallow } from 'enzyme';
import Button from '../Button';

describe('<Button />', () => {
  it('render without exploding', () => {
    expect(shallow(<Button />).length).toEqual(1);
  });

  it('render button with passed text', () => {
    const shallowWrapper = shallow(
      <Button 
        buttonText="Testing alert view" 
        iconLeft="fas fa-user" 
      />
    );

    expect(shallowWrapper.html()).toEqual(
      '<button type="button" title="" class="btn btn-primary btn-space col-md-2">Testing alert view</button>'
    );
  });

  it('mount render button with onclick simulate', () => {
    const shallowWrapper = mount(
      <Button 
        buttonText="Testing alert view" 
        iconLeft="fas fa-user"
        onClick={() => {} }
      />
    );

    shallowWrapper.find('button').simulate('click');
  });
});