jest.unmock('../Paragraph');

import React from 'react';
import { shallow } from 'enzyme';
import Paragraph from '../Paragraph';

describe('<Paragraph />', () => {
  it('render without exploding', () => {
    expect(shallow(
        <Paragraph 
            text="testing" 
        />
    ).length).toEqual(1);
  });
});