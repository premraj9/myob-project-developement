jest.unmock('../Icon');

import React from 'react';
import { shallow } from 'enzyme';
import Icon from '../Icon';

describe('<Icon />', () => {
  it('render without exploding', () => {
    expect(shallow(<Icon iconClass="fas fa-users"/>).length).toEqual(1);
  });

  it('renders a <label> with a passed text', () => {
    const shallowWrapper = shallow(
        <Icon iconClass="fas fa-users"/>
    );

    expect(shallowWrapper.html()).toEqual(
      '<i class="fas fa-users"></i>'
    );
  });
});