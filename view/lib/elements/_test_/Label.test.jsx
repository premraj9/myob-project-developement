jest.unmock('../Label');

import React from 'react';
import { shallow } from 'enzyme';
import Label from '../Label';

describe('<Label />', () => {
  it('render without exploding', () => {
    expect(shallow(
        <Label 
            labelText="testing" 
        />
    ).length).toEqual(1);
  });
});