jest.unmock('../AlertMessage');

import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import AlertMessage from '../AlertMessage';
import Label from '../Label';
import Link from '../Link';

describe('<AlertMessage />', () => {
  it('Find alert component have Label Elemenet component', () => {
    const shallowWrapper = shallow(
      <AlertMessage 
        message="Testing alert view" 
        alertColor="danger" 
        textColor="text-white" 
      />
    );

    expect(shallowWrapper.find(Label)).to.have.lengthOf(1);
  });

  it('Find alert component have Link Elemenet component', () => {
    const shallowWrapper = shallow(
      <AlertMessage 
        message="Testing alert view" 
        alertColor="danger" 
        textColor="text-white" 
        closeClick={() => {}}
      />
    );

    expect(shallowWrapper.find(Label)).to.have.lengthOf(1);
    expect(shallowWrapper.find(Link)).to.have.lengthOf(1);
  });
});