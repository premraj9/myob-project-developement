jest.unmock('../InputField');

import React from 'react';
import { shallow } from 'enzyme';
import InputField from '../InputField';

describe('<InputField />', () => {
  it('render without exploding', () => {
    expect(shallow(
        <InputField 
            inputName="testing" 
        />
    ).length).toEqual(1);
  });
});