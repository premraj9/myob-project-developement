jest.unmock('../Heading');

import React from 'react';
import { shallow } from 'enzyme';
import Heading from '../Heading';

describe('<Heading />', () => {
  it('render without exploding', () => {
    expect(shallow(<Heading text="Testing"/>).length).toEqual(1);
  });

  it('renders a <label> with a passed text', () => {
    const shallowWrapper = shallow(
      <Heading 
        text="Testing alert view" 
      />
    );

    expect(shallowWrapper.html()).toEqual(
      '<h1 class="">Testing alert view</h1>'
    );
  });
});