jest.unmock('../ImagePreview');

import React from 'react';
import { shallow } from 'enzyme';
import ImagePreview from '../ImagePreview';

describe('<ImagePreview />', () => {
  it('render without exploding', () => {
    expect(shallow(
        <ImagePreview 
          imageWrapperClass="col-md-12" 
          imageClass="col-12 col-md-12" 
          imageSrc="core/assets/images/nodatafound.png"
        />
    ).length).toEqual(1);
  });
});