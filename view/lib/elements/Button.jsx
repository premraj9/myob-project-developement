import * as React from 'react';
import Classnames from 'classnames';
import { Link as LinkUrl } from 'react-router-dom';
import Icon from './Icon';

// @flow
type ButtonProps = {
  buttonClass?: string,
  buttonType?: string,
  buttonText?: string,
  hoverTitle?: string,
  buttonSize?: number,
  iconClass?: string,
  iconPosition?: string,
  buttonOffset?: number,
  parameter?: number | string | Array<mixed>,
  onClick?: (param: number | string) => void,
  linkTo?: string, // link to different component
  disabled?: boolean, // make the button clickable or not
  responsive?: boolean, // normal view or responsive button
};

type ButtonStates = {
  offset?: string | empty, // move the container from 0 point
  iconRight?: React.Node, // show icon on right
  iconLeft?: React.Node, // show icon on left
};

/**
 * Button element component view.
 *
 * @author   Premraj Tharmarajah
 * @since    16 August 2018
 */
class Button extends React.Component<ButtonProps, ButtonStates> {
  static defaultProps = {
    buttonClass: 'btn-primary btn-space',
    buttonType: 'button',
    buttonText: 'SUBMIT',
    hoverTitle: '',
    buttonSize: 2,
    buttonOffset: 0,
    iconClass: '',
    iconPosition: 'left',
    parameter: [],
    linkTo: '',
    disabled: false,
    responsive: true,
  };

  constructor(props: ButtonProps) {
    super(props);
    this.state = {
      iconRight: '',
      iconLeft: '',
      offset: '',
    };
  }

  /**
   * react lifecycle to run after render the view
   */
  componentDidMount() {
    this.updateState();
  }

  /**
   * trigger the function when click on the button 
   * pass the parameter to parent component
   */
  onClick = () => {
    this.props.onClick(this.props.parameter);
  };

  /**
   * Update this class when view needs any changes
   */
  updateState() {
    if (this.props.buttonOffset && this.props.buttonOffset !== 0) {
      this.setState({ offset: ` col-md-offset-${this.props.buttonOffset}` });
    }

    if (this.props.iconClass) {
      if (this.props.iconPosition === 'right') {
        this.setState({ iconRight: <Icon IconClass={this.props.iconClass} /> });
      } else {
        this.setState({ iconLeft: <Icon IconClass={this.props.iconClass} /> });
      }
    } else {
    }
  }

  /**
   * react Lifecycle to render view
   */
  render() {
    // use classname react plugin to use css condition
    const classNames = Classnames(
      `btn ${this.props.buttonClass}`,
      this.state.offset ? this.state.offset : '',
      this.props.buttonSize ? this.props.responsive
          ? `col-md-${this.props.buttonSize}`
          : `width-${this.props.buttonSize}`
          : ''
    );

    // react router component change render
    if (this.props.linkTo) {
      const componentReplace = {
        pathname: this.props.linkTo,
        state: this.props.parameter,
      };

      return (
        <LinkUrl className={classNames} to={componentReplace}>
          {this.state.iconLeft}
          {this.props.buttonText}
          {this.state.iconRight}
        </LinkUrl>
      );
    }

    // onclick button function listener view
    if (this.props.onClick) {
      return (
        <button
          type={'button'}
          role={'link'}
          title={this.props.hoverTitle}
          className={classNames}
          onClick={this.onClick}
          tabIndex={'0'}
          disabled={this.props.disabled}
        >
          {this.state.iconLeft}
          {this.props.buttonText}
          {this.state.iconRight}
        </button>
      );
    }

    // normal button view
    return (
      <button
        type={this.props.buttonType}
        title={this.props.hoverTitle}
        className={classNames}
        disabled={this.props.disabled}
      >
        {this.state.iconLeft}
        {this.props.buttonText}
        {this.state.iconRight}
      </button>
    );
  }
}

export default Button;
