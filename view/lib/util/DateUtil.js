import Moment from 'moment';
import Assign from 'babel-runtime/core-js/object/assign';
import * as DateConstant from 'Core/constant/DateConstant';

class DateUtil {
  /**
   * GetTodays date
   *
   * @param string toFormat - (optional) the amount of days to add, defaults to 1
   *
   * @return string - today`s today
   */
  static getTodaysDate(toFormat = null) {
    if (!toFormat) {
      toFormat = DateConstant.DATABASE_FORMAT;
    }
    return Moment().format(toFormat);
  }

  /**
   * Get Current Year  from today's date
   *
   * @return string - the year
   */
  static getCurrentYear() {
    return Moment().format(DateConstant.DATE_YEAR);
  }

  /**
   * Get Current Year from today's date
   *
   * @return string - the year
   */
  static getCurrentYearAndMonth() {
    return Moment().format(DateConstant.DATE_YEAR_MONTH);
  }

  /**
   * Get Current Month from today's date
   *
   * @return string - the month and date
   */
  static getCurrentMonthAndDate() {
    return Moment().format(DateConstant.DATE_MONTH_AND_DATE);
  }

  /**
   * Converts a date from one format to another.
   * If the from and to formats are not provided then we will convert
   *
   * @param string sDate - the date we want to add days to
   * @param string fromFormat - (optional) the from format, defaults to null
   * @param string toFormat - (optional) the amount of days to add, defaults to 1
   *
   * @return string - the formatted date
   */
  static convertDateToFormat(sDate, fromFormat = null, toFormat = null) {
    if (!toFormat) {
      toFormat = DateConstant.DATABASE_FORMAT;
    }

    if (!fromFormat) {
      fromFormat = DateConstant.DATABASE_FORMAT;
    }

    return Moment(sDate, fromFormat).format(toFormat);
  }

  /**
   * Get Current Month start Date from today's date or given date
   *
   * @param string sDate - (optional) date to get first date of current month
   *
   * @return string - the current month start date
   */
  static getMonthStartDate(sDate = null) {
    if (!sDate) {
      sDate = Moment().format(DateConstant.DATABASE_FORMAT);
    }
    const sStartDate = Moment(sDate)
      .startOf('month')
      .format(DateConstant.DATABASE_FORMAT);

    return sStartDate;
  }

  /**
   * Get Current Month End Date from today's date or given date
   *
   * @param string sDate - (optional) date to get last date of previous month
   *
   * @return string - the previous month end date
   */
  static getMonthEndDate(sDate = null) {
    if (!sDate) {
      sDate = Moment().format(DateConstant.DATABASE_FORMAT);
    }
    const sLastDate = Moment(sDate)
      .endOf('month')
      .format(DateConstant.DATABASE_FORMAT);

    return sLastDate;
  }
}

export default DateUtil;
