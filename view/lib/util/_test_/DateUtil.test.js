import DateUtil from '../DateUtil';

/**
 * test result return true 
 */
it('convert string into boolean true', () => {
  const data = DateUtil.convertDateToFormat('2018-07-12', null, 'DD MMM YYYY');
  expect(data).toEqual('12 Jul 2018');
});

/**
 * test result return false 
 */
it('convert string into boolean false', () => {
  const data = DateUtil.getMonthStartDate('2018-07-12');
	expect(data).toEqual('2018-07-01');
});

/**
 * test result return false 
 */
it('convert string  number into int', () => {
  const data = DateUtil.getMonthEndDate('2018-07-12');
	expect(data).toEqual('2018-07-31');
});