const dev = process.env.NODE_ENV !== 'production';

export const defaultUrl = dev ? 'http://localhost:4000' : 'http://localhost:4000';
