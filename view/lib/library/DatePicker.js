import React from 'react';
import Moment from 'moment';
import ReactDatePicker from 'react-datepicker';
import * as DateConstant from 'Core/constant/DateConstant';
import Icon from '../elements/Icon';

// @flow
type DatePickerProps = {
  inputName: string,
  InputclassName: string,
  startDate?: string,
  selected?: string,
  onChange?: (param: number | string) => void,
  dateFormat?: string,
  iconClass?: string,
  className?: string,
  dateType?: string,
  minDate?: string,
};

type DatePickerStates = {
  startDate: Object<mixed>,
  selected: Object<mixed>,
  minDate: Object<mixed>,
};

/**
 * DatePicker component.
 *
 * @author   Premraj Tharmarajah
 * @since    14 August 2018
 */
class DatePicker extends React.Component<DatePickerProps, DatePickerStates> {
  static defaultProps = {
    dateFormat: DateConstant.DATABASE_FORMAT,
    iconClass: 'far fa-calendar-alt',
    startDate: Moment().format(DateConstant.DATABASE_FORMAT),
    selected: Moment().format(DateConstant.DATABASE_FORMAT),
    dateType: '',
    className: 'date-picker-wrap',
    InputclassName: 'remove-icon wizard-date-picker',
    minDate: Moment(),
    onChange: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      // Set state of startDate to current day
      startDate: Moment(),
      minDate: Moment(),
      selected: Moment(),
    };
  }

  componentWillMount() {
    this.setState({
      // Set state of startDate to current day
      startDate: Moment(this.props.startDate),
      selected: Moment(this.props.selected),
      minDate: Moment(this.props.minDate),
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.startDate !== nextProps.startDate) {
      this.setState({
        // Set state of startDate to current day
        startDate: Moment(nextProps.startDate),
      });
    }

    if (this.props.selected !== nextProps.selected) {
      this.setState({
        selected: Moment(nextProps.selected),
      });
    }

    if (this.props.minDate !== nextProps.minDate) {
      this.setState({
        minDate: Moment(nextProps.minDate),
      });
    }
  }

  handleDateChange = date => {
    console.log(date);
    const formattedDate = Moment(date).format(DateConstant.DATABASE_FORMAT);
    this.props.onChange(formattedDate);
  };

  focusInput = event => {
    let parent = null;
    if (event.target.tagName === 'SPAN') {
      parent = event.target.parentNode.parentNode;
    } else if (event.target.tagName === 'I') {
      parent = event.target.parentNode.parentNode;
    }
    if (parent) {
      parent.getElementsByTagName('INPUT')[0].click();
    }
  };

  render() {
    let icon = '';
    if (this.props.iconClass) {
      icon = <Icon iconClass={this.props.iconClass} />;
    }
    return (
      <div className={this.props.className}>
        <ReactDatePicker
          onChange={this.handleDateChange}
          selected={this.state.selected}
          dateFormat={this.props.dateFormat}
          minDate={this.state.minDate}
          className={`form-control ${this.props.InputclassName}`}
        />
        <a onClick={this.focusInput}>{icon}</a>
      </div>
    );
  }
}

export default DatePicker;
