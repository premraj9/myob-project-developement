import React from 'react';
import Moment from 'moment';
import isEqual from 'lodash/isEqual';
import * as DateConstant from 'Core/constant/DateConstant';
import DateUtil from 'Lib/util/DateUtil';
import DatePicker from '../library/DatePicker';
import Label from '../elements/Label';

// @flow
type DatePickerProps = {
  labelText?: string,
  labelClass?: string,
  labelSize?: number,
  labelType?: string,
  labelTitle?: string,
  labelIconClass?: string,
  onChange?: (date: string, type: string) => void,
  startDate?: string,
  endDate?: string,
  iconClass?: string,
  inputSize?: number,
  inputFromName: string,
  className?: string,
  dateFormat?: string,
  minDate?: Object<mixed>,
  visibleEndDate?: boolean,
};

type DatePickerState = {
  startDate: string,
  endDate: string,
};

/**
 * DatePicker collection component.
 *
 * @author   Premraj Tharmarajah
 * @since    14 August 2018
 */
class DatePickerCollection extends React.Component<DatePickerProps, DatePickerState> {
  static defaultProps = {
    labelText: '',
    labelClass: '',
    labelType: 'label',
    labelTitle: '',
    labelSize: 12,
    labelIconClass: '',
    className: 'date-picker-wrap',
    dateFormat: DateConstant.DATE_FORMAT_DATE_PICKER,
    iconClass: 'far fa-calendar-alt',
    inputSize: 3,
    startDate: Moment().format(DateConstant.DATABASE_FORMAT),
    minDate: Moment().format(DateConstant.DATABASE_FORMAT),
    onChange: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      startDate: Moment().format(DateConstant.DATABASE_FORMAT),
    };
  }

  componentWillMount() {
    this.setState({
      // Set state of startDate to current day
      startDate: this.props.startDate,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.startDate !== nextProps.startDate) {
      this.setState({
        // Set state of startDate to current day
        startDate: nextProps.startDate,
      });
    }
  }

  /**
   * react lifecyle execute when state or props have new values
   *
   * force react to rendar view again if there is any changes in state or props
   *
   * @param object nextProps - new class properties object data
   * @param object nextState - new class state object data
   */
  shouldComponentUpdate(nextProps, nextState) {
    return (
      !isEqual(this.state, nextState) ||
      !isEqual(this.props.dateFormat, nextProps.dateFormat)
    );
  }

  /**
   * when change the start date from date time picker
   * then change the startDate in state also notify to parent component
   * with changed type (start or end)
   *
   * @param string date - new date selected
   */
  handleDateChange = (date) => {
    // used by parent view
    this.props.onChange(date, DateConstant.DATE_START);
    this.setState({
      startDate: date,
    });
  };

  render() {
    return (
      <div className="form-group">
        <div className="datepicker-row-wrap">
          <div className="row">
            {this.props.labelText && (
              <div className={`col-md-${this.props.labelSize} no-padding`}>
                <Label
                  labelClass={this.props.labelClass}
                  labelText={this.props.labelText}
                  type={this.props.labelType}
                  labelTitle={this.props.labelTitle}
                  labelIconClass={this.props.labelIconClass}
                />
              </div>
            )}
            <div className={`col-md-${this.props.inputSize} no-padding`}>
              <div className="input-group">
                {this.state.startDate && (
                  <DatePicker
                    inputName={this.props.inputFromName}
                    id={this.props.inputFromName}
                    selected={this.state.startDate}
                    startDate={this.state.startDate}
                    dateFormat={this.props.dateFormat}
                    onChange={this.handleDateChange}
                    iconClass={this.props.iconClass}
                    className={this.props.className}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DatePickerCollection;
