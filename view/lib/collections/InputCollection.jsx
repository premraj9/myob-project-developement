import React from 'react';
import InputField from 'Lib/elements/InputField';
import Paragraph from 'Lib/elements/Paragraph';

// @flow
type Props = {
  formGroupClass?: string,
  errorMessage?: string,
  required?: boolean,
  inputType?: string,
  inputValue?: string,
  inputName: string,
  inputClass?: string,
  inputSize?: number,
  inputId?: string,
  inputIconClass?: string,
  inputDisabled?: boolean,
  inputWrapperClass?: string,
  inputAutoComplete?: 'current-password' | 'on' | 'off' | 'username',
  paragraphSize?: number,
  placeholder?: string,
  paragraphClass?: string,
  paragraphText?: string,
  paragraphIconClass?: string,
  paragraphWrapperClass?: string,
  subParagraphClass?: string,
  subParagraphText?: string,
  subParagraphIconClass?: string,
  subParagraphWrapperClass?: string,
  onBlur?: ({ event: SyntheticFocusEvent<>, value: string }) => void,
  onFocus?: ({ event: SyntheticFocusEvent<>, value: string }) => void,
  onChange: ({ event: SyntheticInputEvent<>, value: string }) => void,
};

// default props for this collection
const defaultProps = {
  formGroupClass: '',
  inputType: 'text',
  inputId: '',
  inputValue: '',
  inputClass: '',
  inputSize: 12,
  required: false,
  inputIconClass: '',
  inputDisabled: false,
  inputWrapperClass: '',
  inputAutoComplete: 'off',
  errorMessage: '',
  paragraphSize: 12,
  placeholder: '',
  paragraphClass: '',
  paragraphText: '',
  paragraphIconClass: '',
  paragraphWrapperClass: '',
  subParagraphClass: '',
  subParagraphText: '',
  subParagraphIconClass: '',
  subParagraphWrapperClass: '',
  onBlur: () => {},
  onFocus: () => {},
};

/**
 * Input Field collection.
 *
 * @author   Premraj Tharmarajah
 * @since    14 August 2018
 */
function InputCollection(props: Props) {
  let subText = '';
  // created for to show sub text
  if (props.subParagraphText !== '') {
    subText = (
      <Paragraph
        class={props.subParagraphClass}
        text={props.subParagraphText}
        iconClass={props.subParagraphIconClass}
        wrapperClass={props.subParagraphWrapperClass}
      />
    );
  }

  let mainText = '';
  // created for to show main text
  if (props.paragraphText !== '') {
    mainText = (
      <Paragraph
        class={props.paragraphClass}
        text={props.paragraphText}
        iconClass={props.paragraphIconClass}
        wrapperClass={props.paragraphWrapperClass}
      />
    );
  }

  return (
    <div className={`form-group ${props.formGroupClass}`}>
      <div className={`no-padding col-md-${props.paragraphSize}`}>
        {mainText}
        {subText}
      </div>
      <div className={`no-padding col-md-${props.paragraphSize} `}>
        <InputField
          inputClassName={props.inputClass}
          inputValue={props.inputValue ? props.inputValue : ''}
          inputName={props.inputName}
          inputType={props.inputType}
          inputSize={props.inputSize}
          placeholder={props.placeholder}
          iconClass={props.inputIconClass}
          disabled={props.inputDisabled}
          wrapperClass={`${props.inputWrapperClass} no-padding`}
          autoComplete={props.inputAutoComplete}
          errorMessage={props.errorMessage}
          required={props.required}
          onChange={props.onChange}
          onBlur={props.onBlur}
          onFocus={props.onFocus}
        />
      </div>
    </div>
  );
}

InputCollection.defaultProps = defaultProps;

export default InputCollection;
