const webpack = require("webpack");
const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const WorkboxPlugin = require("workbox-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const occurrenceOrderPlugin = new webpack.optimize.OccurrenceOrderPlugin();
const hotModuleReplacementPlugin = new webpack.HotModuleReplacementPlugin();
const cleanWebpackPlugin = new CleanWebpackPlugin(["dist"]);

const htmlWebpackPlugin = new HtmlWebPackPlugin({
  template: `${__dirname}/public/index.html`,
  filename: "index.html",
  inject: "body"
});

// return true if envorinment is production
const devMode = process.env.NODE_ENV !== "production";

// For later use
const copyWebpackPlugin = new CopyWebpackPlugin(
  [
    { from: "../public/manifest.json", to: "manifest.json" }
  ],
  { copyUnmodified: true }
);

module.exports = (env, argv) => {
  /**
   * In package json under scripts we set the --env.NODE_ENV= variable.
   * If we are in development set the debug variable to true.
   * @type {Boolean}
   */
  let debug = env ? env.NODE_ENV !== "production" : true;

  /*
   * Because of env, now we have a function and we need to return the
   * configuration.
   */
  return {
    /**
     * This is the base directory, an absolute path, for resolving entry points
     * and loaders from configuration.
     * @type {String}
     */
    context: path.resolve(__dirname, "src"), // maybe just __dirname
    /**
     * If env is debug do inline-sourcemapping which helps console logging
     * https://webpack.js.org/configuration/devtool/#devtool
     * @type {[type]}
     */
    devtool: debug ? "inline-sourcemap" : false,
    /**
     * Webpack dev server, by adding /webpack-dev-server to the url we can see
     * where the files are being served from, example:
     * localhost:8888/webpack-dev-server
     * @type {Object}
     */
    devServer: {
      /**
       * With contentBase we can tell the server where to serve content from.
       * This is only necessary if we want to serve static files.
       * @type {String}
       */
      // contentBase: path.resolve(__dirname, "dist"),
      /**
       * Watch for changes on all the files under contentBase.
       * @type {Boolean}
       */
      watchContentBase: true,
      /**
       * In order to enable gzip compression for everything served we need to
       * set the compress option.
       * @type {Boolean}
       */
      compress: true,
      /**
       * We can set deploy port number for the application in the
       * webpack-dev-server.
       * @type {Number}
       */
      port: 8896,
      /**
       * We can control what bundle information is displayed. To show only errors
       * more info on stats options https://webpack.js.org/configuration/stats
       * @type {String}
       */
      stats: "errors-only",
      /**
       * If we want dev-server to open the app at the first time in our browser
       * and just refresh afterwards while we change our code we can use the
       * open option.
       * @type {Boolean}
       */
      open: true,
      /**
       * Inline option adds “Live reloading” for the entire page.
       * @type {Boolean}
       */
      inline: true,
      /**
       * Hot option enables “Hot Module Reloading” HMR that tries to reload just
       * the component that’s changed. By setting both inline and hot options,
       * HMR will be done first and if that is not enough the entire page will
       * reload because of the inline option.
       * @type {Boolean}
       */
      hot: true,
      /**
       * In order to be able to use react-router-dom with BrowserRouter this
       * option has to be set to true, reference:
       * https://tylermcginnis.com/react-router-cannot-get-url-refresh/
       * @type {Boolean}
       */
      historyApiFallback: true
    },
    /**
     * Entry source, where magic happens Q.Q
     * @type {Array}
     */
    entry: ["babel-polyfill", "./index.js"], // maybe ./src/index.js
    /**
     * Output option has the path to the folder where we will create the new
     * bundle with name as filename.
     * @type {Object}
     */
    output: {
      /**
       * Path tells Webpack where it should store the result.
       * We can make use of output path using nodeJs path module or just do
       * string concatenation (using path is preffered).
       * path.resolve https://nodejs.org/dist/latest-v6.x/docs/api/path.html#path_path_resolve_paths
       * path.join https://nodejs.org/dist/latest-v6.x/docs/api/path.html#path_path_join_paths
       * @type {String}
       */
      path: path.resolve(__dirname, "dist"),
      /**
       * publicPath is used by Webpack plugins to update the URLs inside CSS,
       * HTML files when generating production builds.
       * We need to set the public path option to the folder that stores the bundle
       * https://webpack.js.org/configuration/dev-server/#devserver-publicpath-
       * @type {String}
       */
      publicPath: "",
      /**
       * Naming with multiple bundles for multiple entry points is possible:
       * using the entry name "[name].bundle.js"
       * using the hash based on each chunk's content "[chunkhash].bundle.js"
       * more on https://webpack.js.org/configuration/output/#output-filename
       * @type {String}
       */
      filename: "js/bundle.js",
      chunkFilename: "js/[id].[hash].bundle.js"
    },
    /**
     * This option determines how the different types of modules in the project
     * will be treated https://webpack.js.org/configuration/module/
     * @type {Object}
     */
    module: {
      /**
       * Every element of the rules option array is an object containing
       * individual loaders and their respective configurations.
       * @type {Array}
       */
      rules: [
        // !!! Keep this at 1st place, prod settings modify this
        {
          test: /\.(woff|woff2|eot|ttf|svg)(\?v=[a-z0-9]\.[a-z0-9]\.[a-z0-9])?$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "[name].[ext]"
              }
            }
          ]
        },
        // Babel loader
        {
          /**
           * Required option, the loader needs to know which file extension it’s
           * going to work with.
           * @type {RegExp}
           */
          test: /\.(js|jsx?)$/,
          /**
           * Optional, in order to save a lot of memory and execution time we
           * don't need to parse modules from a specific directory.
           * @type {RegExp}
           */
          exclude: /(node_modules|functions)/,
          /**
           * Required option, the rule must have a loader property as a string.
           * Here we set the loaders we want to use.
           * Loaders can be chained by passing multiple loaders, they will be
           * applied from right to left (last to first configured).
           * They can have options property as a string or object.
           * This value is passed to the loader, which should interpret it as
           * loader options https://webpack.js.org/configuration/module/#module-rules
           * @type {Object}
           */
          use: {
            loader: "babel-loader",
            options: {
              presets: ["react", "es2015", "stage-2"],
              plugins: [
                "transform-class-properties",
                "transform-object-rest-spread"
              ]
            }
          }
        },
        // HTML loader
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader",
              options: {
                /**
                 * This option will minimize the .html files, like UglifyJs does
                 * to .js files https://webpack.js.org/loaders/html-loader/#examples
                 * @type {Boolean}
                 */
                minimize: !debug // if in production mode minimize html file
              }
            }
          ]
        },
        // SCSS, CSS loaders
        {
          test: /\.(c|sc|sa)ss$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: "css-loader"
            },
            {
              loader: "sass-loader"
            }
          ]
        },
        // File loader for pictures
        {
          test: /\.(jpe?g|png|gif)(\?v=[a-z0-9]\.[a-z0-9]\.[a-z0-9])?$/,
          use: [
            {
              loader: "url-loader",
              options: {
                limit: 10000, // return DataURL if image size <= 8KB
                name: "./core/assets/images/[name].[ext]",
                fallback: "file-loader" // use file loader for size > 8KB
              }
            }
          ]
        }
      ]
    },
    // Setting up the Core and Library as alias
    resolve: {
      alias: {
        Core: path.resolve(__dirname, "./core"),
        Lib: path.resolve(__dirname, "./lib")
      },
      extensions: ["*", ".js", ".jsx"]
    },
    /**
     * Since Webpack 4 optimizations are removed from the plugin option, now
     * we need to setup the optimization option.
     * @type {Object}
     */
    optimization: debug
      ? {}
      : {
          /**
           * For production we can uglify the .js files.
           * @type {Boolean}
           */
          minimize: true,
          // Split code into separate files
          splitChunks: {
            // include all types of chunks
            chunks: "all"
          }
        },
    /**
     * The plugins option is used to customize the Webpack build process in
     * different ways. We can run different plugins depending on the environment.
     * @type {Array}
     */
    plugins: debug
      ? [
          /**
           * Enable hot option under devServer.
           * @type {Object}
           */
          hotModuleReplacementPlugin,
          /**
           * Enable .html files script bundling and minimization (check html-loader).
           * @type {Object}
           */
          htmlWebpackPlugin,
          /**
           * Setting the environment plugin
           * @type {Object}
           */
          new webpack.EnvironmentPlugin({
            NODE_ENV: "development",
            DEFAULT_HOST: "http://localhost:4000"
          }),
          /**
           * Copy static files such as manifest.json file and icons.
           * @type {Object}
           */
          // copyWebpackPlugin,
          /**
           * Create custom service worker with workbox's inject manifest function.
           * This plugin must be set as last plugin. From official docs:
           * Since Workbox revisions each file based on its contents,
           * Workbox should always be the last plugin you call.
           * @type {Object}
           */
          new WorkboxPlugin.InjectManifest({
            swSrc: "./src/sw.js",
            swDest: "sw.js"
          }),
          new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[hash].css",
            chunkFilename: "[contenthash].css"
          })
        ]
      : [
          /**
           * We can clear the content from our dist folder before every build:prod
           * by utilising the clean-webpack-plugin https://github.com/johnagan/clean-webpack-plugin
           * @type {Object}
           */
          cleanWebpackPlugin,
          htmlWebpackPlugin,
          /**
           * Setting the environment plugin
           * @type {Object}
           */
          new webpack.EnvironmentPlugin({
            NODE_ENV: "production",
            DEFAULT_HOST: "http://localhost:4000"
          }),
          /**
           * Official docs https://github.com/webpack/docs/wiki/optimization:
           * Webpack gives our modules and chunks ids to identify them. Webpack can
           * vary the distribution of the ids to get the smallest id length for
           * often used ids with a simple option.
           * @type {Object}
           */
          occurrenceOrderPlugin,
          copyWebpackPlugin,
          new WorkboxPlugin.InjectManifest({
            swSrc: "./src/sw.js",
            swDest: "sw.js"
          }),
          new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].css",
            chunkFilename: "[id].css"
          })
        ]
  };
};
