/*
 * Api Success Code with content return
 */
export const STATUS_CODE_SUCCESS = 200;

/*
 * Api Success Code with no content after put data
 */
export const STATUS_CODE_SUCCESS_PUT = 204;

/*
 * Api Success Code with no content after created new data
 */
export const STATUS_CODE_SUCCESS_CREATED = 201;

/*
 * Api Success Code Max error code
 */
export const STATUS_SUCCESS_MAX = 300;
