/**
 * Loading Message
 */
export const LOADING = 'Loading...';

/**
 * Zero integer value
 */
export const ZERO = 0;

/**
 * Default symbol for question mark
 */
export const SYMBOL_QUESTION = '?';

/**
 * Default symbol for and sign
 */
export const SYMBOL_AND = '&';